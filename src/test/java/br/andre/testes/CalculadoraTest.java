package br.andre.testes;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.util.Assert;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Flaky;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;

@SpringBootTest
@Epic("Allure examples")
@Feature("Junit 4 support")
public class CalculadoraTest {

	Integer valor1 = 0;
	Integer valor2 = 0;
	Integer resultado = 0;

	@Flaky
	@Test
	@DisplayName("CT S-SRIFU01 - Sucesso ao Somar")
	@Description("Soma de valores.")
	@Story("HIS-Utilizar Calculadora")
	@Link(name = "Minha História 1", type = "https://example.org")
    @Severity(SeverityLevel.CRITICAL)
	@Step("Passo 1.")
	public void soma() {

		this.valor1 = 10;
		this.valor2 = 2;
		this.resultado = this.valor1 + this.valor2;
		System.out.println(this.resultado);
//		Assert.assertTrue(this.resultado > 100);
		Assert.isTrue(this.resultado > 1);

//		SoftAssertions assertiva = new SoftAssertions();

//		assertiva.assertThat(this.resultado).isGreaterThan(100);

//		softAssertions(this.resultado > 0);
		System.out.println("continuando soma xxxxxxxxxxxxxxx");
	}

	@Flaky
	@Test
	@DisplayName("Subtrair.")
	@Description("Subtração de valores.")
	@Story("HIS-Utilizar Calculadora")
	@Link(name = "Minha História 1", type = "https://example.org")
	@Severity(SeverityLevel.CRITICAL)
	@Step("Passo 2.")
	public void subtracao() {

		this.valor1 = 10;
		this.valor2 = 2;
		this.resultado = this.valor1 + this.valor2;
		System.out.println(this.resultado);

		SoftAssertions assertiva = new SoftAssertions();
		assertiva.assertThat(this.resultado).isLessThan(100);

//		softAssertions(this.resultado > 0);
		System.out.println("continuando subtração yyyyyyyyyyyyyyy");
	}
	
	@Disabled
	@Test
	@DisplayName("Multiplicar.")
	@Description("Multiplicação de valores.")
	@Story("HIS-Utilizar Calculadora")
	@Link(name = "Minha História 2", type = "https://example.org")
	@Severity(SeverityLevel.MINOR)
	@Step("Passo 3.")
	public void multiplicação() {

		this.valor1 = 10;
		this.valor2 = 2;
		this.resultado = this.valor1 * this.valor2;
		System.out.println(this.resultado);

		SoftAssertions assertiva = new SoftAssertions();
		assertiva.assertThat(this.resultado).isLessThan(100);

		System.out.println("continuando multiplicação");
	}


}
