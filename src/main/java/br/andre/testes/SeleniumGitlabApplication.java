package br.andre.testes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeleniumGitlabApplication.class, args);
	}

}
