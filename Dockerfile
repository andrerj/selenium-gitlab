FROM maven:3.6-jdk-8-slim
MAINTAINER  Andre Vieira <andre.vieira-de-lima@serpro.gov.br>

WORKDIR /dejavu
COPY src /dejavu/src
COPY pom.xml /dejavu

# instala e atualiza recursos do linux 
RUN apt-get -y update
RUN apt-get install apt-utils -y
RUN apt-get install wget -y
RUN apt-get install dialog apt-utils -y
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get install -y -q
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install curl
RUN apt-get install -y gnupg2
RUN apt-get install unzip -y

# instala o Google Chrome
# RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
# RUN apt install ./google-chrome-stable_current_amd64.deb -y
RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add  
RUN bash -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google-chrome.list" 
RUN apt-get -y update 
RUN apt-get -y install google-chrome-stable
 
# instala o Chrome Driver para Slelenium
RUN wget https://chromedriver.storage.googleapis.com/94.0.4606.61/chromedriver_linux64.zip 
RUN unzip chromedriver_linux64.zip
# RUN unzip -version

# instala o Allure Reports
RUN apt-get install -y allure
RUN curl -o allure-2.13.8.tgz -OLs https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.8/allure-commandline-2.13.8.tgz
RUN tar -zxvf allure-2.13.8.tgz -C /opt/
RUN ln -s /opt/allure-2.13.8/bin/allure /usr/bin/allure
RUN allure --version
#RUN mkdir allure-results

# limpa a pasta de resultados
#RUN allure generate --clean --output /allure-results

# gera relatório offline
#RUN allure generate -c /allure-results

# executa os testes
#RUN mvn clean test
#RUN allure serve --host localhost --port 8080
# RUN allure serve /allure-results --port 9999

# -------------------------------------
#FROM java:8
#FROM centos:centos7

#MAINTAINER  Andre Vieira <andre.vieira-de-lima@serpro.gov.br>

# Prerequisites.

#RUN yum -y update
#RUN yum -y install wget tar

#WORKDIR /app


#ADD /target/dejavu-1.jar dejavu-1.jar

#resolve maven dependencies
# RUN mvn clean package -Dmaven.test.skip -Dmaven.main.skip -Dspring-boot.repackage.skip && rm -r target/

#ENTRYPOINT ["java","-jar","dejavu-1.jar"]

#FROM openjdk:8-jdk-alpine

#MAINTAINER  Andre Vieira <andre.vieira-de-lima@serpro.gov.br>

#RUN apt-get -y update
#RUN apt-get install -y allure
#RUN curl -o allure-2.13.8.tgz -OLs https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.8/allure-commandline-2.13.8.tgz
#RUN tar -zxvf allure-2.13.8.tgz -C /opt/
#RUN ln -s /opt/allure-2.13.8/bin/allure /usr/bin/allure
#RUN allure --version

# ARG JAR_FILE=target/*.jar

# cd /usr/local/runme
# WORKDIR /usr/local/runme

# copy target/find-links.jar /usr/local/runme/app.jar
# COPY ${JAR_FILE} app.jar

# copy project dependencies
# cp -rf target/lib/  /usr/local/runme/lib
# ADD ${JAR_LIB_FILE} lib/

# java -jar /usr/local/runme/app.jar
# ENTRYPOINT ["java","-jar","app.jar"]
